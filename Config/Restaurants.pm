package Config::Restaurants;

our $restaurants = {
    'padthai' => {
        'type' => 'zomato',
        'site_id' => 16506806,
    },
    'everest' => {
        'type' => 'zomato',
        'site_id' => 16506537,
    },
    'himalaya' => {
        'type' => 'zomato',
        'site_id' => 18020959,
    },
    'ubilehoberanka' => {
        'type' => 'zomato',
        'site_id' => 16506737,
        'aliases' => [qw(beranek bilyberanek uberanka)],
    },
    'selepka' => {
        'type' => 'zomato',
        'site_id' => 16506040,
        'fallback_url' => 'http://www.selepova.cz/denni-menu/',
    },
    'magistr' => {
        'type' => 'zomato',
        'site_id' => 16506840,
    },
    'suzies' => {
        'type' => 'zomato',
        'site_id' => 16506939,
    },
    'upavlinky' => {
        'type' => 'zomato',
        'site_id' => 16507331,
        'aliases' => [qw(pavlinka)],
    },
    'cattani' => {
        'type' => 'zomato',
        'site_id' => 16506435,
        'aliases' => [qw(catani katani)],
    },
    'udrevaka' => {
        'type' => 'zomato',
        'site_id' => 16505458,
        'aliases' => [qw(drevak)],
    },
    'freshfreaks' => {
        'type' => 'zomato',
        'site_id' => 16507572,
    },
    'vegalite' => {
        'type' => 'zomato',
        'site_id' => 16506673,
    },
    'espanola' => {
        'type' => 'zomato',
        'site_id' => 16505872,
        'aliases' => [qw(espagnola)],
        'fallback_url' => 'http://www.espanola.cz/denni-menu/',
    },
    'kupe' => {
        'type' => 'zomato',
        'site_id' => 16506303,
    },
    'goldennepal' => {
        'type' => 'zomato',
        'site_id' => 18346442,
        'fallback_url' => 'http://goldennepal.cz/denni-menu/',
    },
    'divabara' => {
        'type' => 'zomato',
        'site_id' => 16514047,
        'fallback_url' => 'http://www.restauracedivabara.cz/poledni-menu',
    },
    'annapurna' => {
        'type' => 'showurl',
        'menu_url' => 'http://indicka-restaurace-annapurna.cz/index.php?option=com_content&view=article&id=2&Itemid=118',
        'aliases' => [qw(anapurna)],
    },
    'tivoli' => {
        'type' => 'showurl',
        'menu_url' => 'http://tivolicafe.cz/menu/poledni-menu/',
    },
    'alcapone' => {
        'type' => 'module',
        'module' => 'Obedar::AlCapone',
        'fallback_url' => 'http://www.pizzaalcapone.cz/poledni-menu.html',
    },
    'lightofindia' => {
        'type' => 'module',
        'module' => 'Obedar::LightOfIndia',
        'fallback_url' => 'http://lightofindia.cz/lang-cs/denni-menu',
        'aliases' => [qw(light)],
    },
    'sabaidy' => {
        'type' => 'zomato',
        'site_id' => 16512604,
        'fallback_url' => 'http://www.amphone.eu/poledni-menu',
    },
    'lastrada' => {
        'type' => 'module',
        'module' => 'Obedar::LaStrada',
        'menu_url' => 'http://lastrada-brno.cz/poledni-menu/',
    },
    'zappadlo' => {
        'type' => 'module',
        'module' => 'Obedar::Zappadlo',
        'menu_url' => 'http://www.zappadlo.cz/denni-menu/',
        'aliases' => [qw(zapadlo)],
    },
    'nepal' => {
        'type' => 'module',
        'module' => 'Obedar::NepalBrno',
        'menu_url' => 'http://nepalbrno.cz/weekly-menu/',
    },
    'klubcestovatelu' => {
        'type' => 'module',
        'module' => 'Obedar::KlubCestovateluBrno',
        'fallback_url' => 'http://www.hedvabnastezka.cz/klub-cestovatelu-brno/poledni-menu-2/',
    },
    'vitalite' => {
        'type' => 'module',
        'module' => 'Obedar::Vitalite',
        'fallback_url' => 'http://www.vitalite.cz/',
    },
    'avatar' => {
        'type' => 'module',
        'module' => 'Obedar::Avatar',
        'fallback_url' => 'https://sites.google.com/site/avatarbrno/menu',
    }
    'buddha' => {
        'type' => 'module',
        'module' => 'Obedar::Buddha',
        'fallback_url' => 'http://www.buddhabrno.cz/',
    },
    'khaybar' => {
        'type' => 'showurl',
        'menu_url' => 'https://www.facebook.com/people/Khaybar-Indick%C3%A1-Restaurace/100012432312966',
    },
    'comodo' => {
        'type' => 'module',
        'module' => 'Obedar::Comodo',
        'fallback_url' => 'http://comodorestaurant.cz/denni-menu',
    },
    'rybenka' => {
        'type' => 'module',
        'module' => 'Obedar::Rybenka',
        'fallback_url' => 'http://www.rybenka.cz/',
    },
    'haribol' => {
        'type' => 'nomenu',
    },
    'chutnestesti' => {
        'type' => 'nomenu',
    },
    'lokofu' => {
        'type' => 'nomenu',
    },
    'pho' => {
        'type' => 'nomenu',
    },
    'ellas' => {
        'type' => 'nomenu',
    },
    'ukarla' => {
        'type' => 'nomenu',
        'aliases' => [qw(karel)],
    },
    # Hoa Mai (byvala Kolocava)
    'hoamai' => {
        'type' => 'nomenu',
        'aliases' => [qw(kolocava)],
    },
};

1;
