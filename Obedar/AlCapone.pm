package Obedar::AlCapone;

# Pizzeria Al Capone Brno.

use open ':std', ':utf8';

use strict;
use utf8;
use LWP::Simple;
use POSIX;

# vlastni jmeno/oznaceni
sub my_name {
	return 'alcapone';
}

# URL menicka
sub my_url {
	return 'http://www.pizzaalcapone.cz/denni-menu';
}

# get_menu
sub get_menu {
	my ($day) = @_;
	my $menu;

	# pozadovany den
	my $now_re = POSIX::strftime('%d.\s*%m.\s*%Y', localtime( time + $day*86400 ));

	# get menu
	my $html = get( my_url );

	# ceny
	my (%ceny) = $html =~ m#(Menu 1) - (\d+\s*Kč) / (Menu 2) - (\d+\s*Kč) / (Menu 3) - (\d+\s*Kč)#;

	# regex magic
	# vyhazet balas na zacatku
	$html =~ s/.*<div class="normal">\s*//s;
	# ... a na konci
	$html =~ s/\s*<\/div>.*//s;
	# BOM uprostred html?
	$html =~ s/\x{feff}//g;
	$html =~ s/.*<\/h2>\s*//s;
	# zahodit konce radku
	$html =~ s/>\r?\n/>/g;
	$html =~ s/<br>\r?\n/<br>/g;
	# za kazdym koncovym p novy radek
	$html =~ s/\/p>/\/p>\n/g;

	# vytahnout dnesni/pozadovany den
	my ($dnes) = $html =~ /^(.*$now_re.*)$/m;

	my (@d) = grep { $_ } map { $_ =~ s/<\/?[bp]>//g; $_; } split(/<br>/, $dnes);
	shift(@d); # zahodit uvodni den

	# doplnit ceny
	$menu = join("\n", map { my ($m) = $_ =~ /(Menu\s*\d+)/; if ( $m ) { $_ .= " [$ceny{$m}]"; } $_; } @d);

	return (0, $menu);
}

1;
