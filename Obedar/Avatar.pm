package Obedar::Avatar;

use open ':std', ':utf8';

use strict;
use utf8;
use LWP::Simple;
use POSIX;

# my name
sub my_name {
    return 'avatar';
}

# my URL
sub my_url {
    return 'https://sites.google.com/site/avatarbrno/menu';
}

# get_menu
sub get_menu {
    my ($day) = @_;
    my $menu;
    # get html/xml/source
    my $html = get( my_url );
    # fail :/
    return (1, 'parsing has not saved the day today') unless $html;
    # do your magic here
    $html =~ s/.*(<[^>]*>CENA MENU)/$1/s;
    $html =~ s/(BEZLEPEK<\/span>).*/$1/s;

    $html =~ s/([VB])<\/span>/$1%CRLF%/g;

    $html =~ s/<\/?(?:font)[^>]*>//g;
    $html =~ s/<div[^>]*>/<div>/g;

    $html =~ s/\n/ /sg;

    my ($cena) = $html =~ /^(CENA MENU.*100g)/;
    $cena =~ s#</div><div># #;

    $html =~ s#(PO/MO|ÚT/TU|ST/WE|ČT/TH|PÁ/FR)#\n$1 #g;

    $html =~ s/<\/?(?:font|b|p|span|a|em)[^>]*>//g;
    $html =~ s/<\/?(?:div|span|b|u)[^>]*>/ /g;
    $html =~ s/<img[^>]*>//sg;

    $html =~ s/DNES//g;

    $html =~ s/-V\s=\sVEGAN/%CRLF%%CRLF%-V = VEGAN/;
    $html =~ s/-B\s=\sBEZLEPEK/%CRLF%-B = BEZLEPEK/;

    my $den = {
	1 => 'PO/MO',
	2 => 'ÚT/TU',
	3 => 'ST/WE',
	4 => 'ČT/TH',
	5 => 'PÁ/WE',
    }->{+(localtime( time + $day*86400 ))[6]};

    my ($den_mes, $mesic) = +(localtime( time + $day*86400 ))[3,4];
    $mesic++;
    my $datum = "$den_mes\\.$mesic\\.";

    my $den_re = "$den\\s*".$datum; #POSIX::strftime('%d\.\s*%m\.\s*', localtime);

    ($menu) = $html =~ /^$den_re(.*)/m;

#    $menu .= "\n\n" . $html;

    $menu =~ s/%CRLF%/\n/g;
    $menu =~ s/^\s+//mg;
    $menu =~ s/\t/ /g;
    $menu =~ s/ +/ /mg;

    $menu .= "\n-V = VEGAN\n-B = BEZLEPEK";

    $menu = $cena . "\n\n" . $menu;

    # success!
    return (0, $menu);
}

1;

# vim: softtabstop=4
