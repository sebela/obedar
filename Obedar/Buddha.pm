package Obedar::Buddha;

# vim: tabstop=4 softtabstop=4 expandtab shiftwidth=4

use strict;
use utf8;
use LWP::Simple;
use POSIX;
use HTML::TreeBuilder::XPath;

# my name
sub my_name {
    return 'buddha';
}

# my URL
sub my_url {
    return 'http://www.buddhabrno.cz/';
}

# get_menu
sub get_menu {
    my ($day) = @_;
    my $menu;
    # get html/xml/source
    my $html = get(my_url());
    # do your magic here

    my $now_dm = strftime('%d.%m.', localtime(time + $day*86400));

    my $tree = HTML::TreeBuilder::XPath->new();
    $tree->parse_content($html);
    my $list = $tree->findvalue('//div[@id="jidelnilisteklong"]');

    # TODO co so sviatkami? co z tohto sa rozbije?
    my @weekly_menu = split /PONDĚLÍ|ÚTERÝ|STŘEDA|ČTVRTEK|PÁTEK|Menu PO - PÁ/, $list;
    shift @weekly_menu;
    pop @weekly_menu;

    my $menu = {};

    foreach my $daily_menu (@weekly_menu) {

        # v tomto bode mame typicky nieco taketo:
        # ''' /Tuesday/ 20. 9.:   Polévka: 0,2l Dal Soup (polévka ze žlutého hrachu, A: 1) 17,- Kč  150g Chicken Fal (kuřecí kousky ve velmi ostré omáčce Fal, A:7) 89,- Kč  150g Chicken Achari (kuřecí kousky v omáčce s mangem&chilli papričkami, A: 7) 89,- Kč VEG 150g Baigan Bharta (Tandoor lilek s ind. kořením, cibulkou a rajčaty, A: 7) 89,- Kč  150g Mix Thali (Mix the daily menu, A:7) 120,- Kč Příloha ke každému jídlu (v ceně): Tandoori Nan (indický chléb, A:1+3) / indická rýže Basmati / kombinace obou příloh. Polévka se podává zvlášť/soup is served separately from menu. ALERGENY: 1: obiloviny (lepek), 3: vejce, 7: mléko/ml. výrobky a 8: skořápkové plody (kešu&kokos). Polévky obsahují glutaman a mouku.'''

        $daily_menu =~ m/(\d+)\. *(\d+)./;
        my $date = sprintf "%02d.%02d.", $1, $2;

        $daily_menu =~ s/Příloha.*//;
        $daily_menu =~ s/(\d+),- Kč */[$1 Kč]\n/g;
        $daily_menu =~ s/\d+g //g;
        $daily_menu =~ s/^.*0,2l //;
        $daily_menu =~ s/, A:[0-9+ ]*?\)/)/g;
        $daily_menu =~ s/\n\Z//;

        $menu->{$date} = $daily_menu;
    }

    my $now_menu = $menu->{$now_dm};
    if (not $now_menu) {
        return (1, 'Parsing failed');
    } else {
        return (0, $now_menu);
    }
}

1;
