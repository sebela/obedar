package Obedar::Comodo;

use open ':std', ':utf8';

use strict;
use utf8;
use LWP::Simple;
use POSIX;

# my name
sub my_name {
	return 'comodo';
}

# my URL
sub my_url {
	return 'http://comodorestaurant.cz/denni-menu';
}

# get_menu
sub get_menu {
	my ($day) = @_;
	my $menu;
	# get html/xml/source
	# do your magic here
	my $html = get( my_url );

	$html =~ s/\r//g;
	$html =~ s/.*<div id="coll-02">//s;
	$html =~ s/.*<div class="menu">//s;
	$html =~ s/<div class="clear"><\/div>/\n/g;
	$html =~ s/<div class="menuinfo.*//s;

	$html =~ s/\r?\n//g;
	$html =~ s/<h4>/\n<h4>/g;
	$html =~ s/^\s*//sg;

#print $html, "\n";

	my $den = {
		1 => 'PONDĚLÍ',
		2 => 'ÚTERÝ',
		3 => 'STŘEDA',
		4 => 'ČTVRTEK',
		5 => 'PÁTEK',
	}->{+(localtime( time + $day*86400 ))[6]};

	($menu) = $html =~ /^(<h4>$den.*)/m;
	$menu =~ s/<h4>$den<\/h4>//;
	$menu =~ s/<\/div><div class="menuvariant/<\/div>\n<div class="menuvariant/g;
	$menu =~ s/class="menuprice">([^<]*)</class="menuprice">[$1]</g;
	$menu =~ s/></> </g;
	$menu =~ s/<\/?div[^>]*>//g;
	$menu =~ s/\*//g;

	return (0, $menu);
}

1;
