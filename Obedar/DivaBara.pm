package Obedar::DivaBara;

# vim: tabstop=4 softtabstop=4 expandtab shiftwidth=4

use open ':std', ':utf8';

use strict;
use utf8;
use LWP::Simple;
use POSIX;

# my name
sub my_name {
    return 'divabara';
}

# my URL
sub my_url {
    return 'http://www.restauracedivabara.cz/poledni-menu';
}

# get_menu
sub get_menu {
    my ($day) = @_;
    my $menu;
    # get html/xml/source
    my $html = get( my_url );
    # fail :/
    return (1, 'parsing has not saved the day today') unless $html;
    # do your magic here
    $html =~ s/\r//g;
    $html =~ s/\t/ /g;
    $html =~ s/^.*?<caption/<caption/s;
    $html =~ s/<h4>PŘEJEME VÁM DOBROU.*//s;
    $html =~ s/>\n\s*/>/sg;
    $html =~ s/<\/(table)>/<\/$1>\n/sg;
    $html =~ s/ *\S+=['"][^"']*['"]/ /g;

    $html =~ s/<\/?(?:strong|tbody|th|table)\s*>//sg;
    $html =~ s/<caption/\n<caption/g;
    $html =~ s/ *>/>/g;
    # osamocena cena (menu D)
    $html =~ s/<h4>\s*\n\s*/<h4>/sg;
    $html =~ s/\n<\/h4>/<\/h4>/sg;
    $html =~ s/<h4>\s*/<h4>/sg;
    $html =~ s/[^\S\n]+/ /g;

    my ($menu_d, $cena_d) = $html =~ /^.*MENU D[^<]*<\/h4><h4>([^<]+)<\/h4><h4>([^<]+)/m;
    $menu_d .= ' ['.$cena_d.']';

    my $den = {
        1 => 'PONDĚLÍ',
        2 => 'ÚTERÝ',
        3 => 'STŘEDA',
        4 => 'ČTVRTEK',
        5 => 'PÁTEK',
    }->{+(localtime( time + $day*86400 ))[6]};


    ($menu) = $html =~ /^<caption>$den<\/caption>(.*)$/mi; 
    $menu =~ s/<tr><\/tr>//g;
    $menu =~ s/<\/tr>/<\/tr>\n/g;
    $menu =~ s/<\/?tr>//g;
    $menu =~ s/<td>([^<]+)<\/td>\s*$/[$1]/mg;
    $menu =~ s/<td>//g;
    $menu =~ s/<\/td>/ /g;
    # chybne html u polevky
    if ( $menu =~ /^(\[.*\])$/m ) {
        my $old = $1;
        my $new = $old;
        $new =~ s/[\[\]]//g;
        $menu =~ s/^\Q$old\E$/$new/m;
    }
    $menu =~ s/^\s*//gm;

    # pripojit celotydenni menu D
    $menu .= "\nD " . $menu_d;

    # success!
    return (0, $menu);
}

1;
