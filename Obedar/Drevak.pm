package Obedar::Drevak;

# vim: tabstop=4 softtabstop=4 expandtab shiftwidth=4

use strict;
use utf8;
use LWP::UserAgent;
use POSIX;

our $ua;

# my name
sub my_name {
    return 'drevak';
}

# my URL
sub my_url {
    return 'http://www.udrevaka.cz/denni-menu/';
}

# get_menu
sub get_menu {
    my ($day) = @_;
    my $menu;
    unless ( $ua ) {
        $ua = LWP::UserAgent->new;
        $ua->agent( 'Firefox' );
    }
    # get html/xml/source
    my $html = $ua->get( my_url )->decoded_content;
    # fail :/
    return (1, 'parsing has not saved the day today') unless $html;
    # do your magic here

    $html =~ s/\r//g;
    $html =~ s/^.*(<b>Pondělí.*)<tr><td>Po dobu meníček.*$/$1/s;
    $html =~ s/<b>/\n<b>/gi;

	my $den = {
		1 => 'PONDĚLÍ',
		2 => 'ÚTERÝ',
		3 => 'STŘEDA',
		4 => 'ČTVRTEK',
		5 => 'PÁTEK',
	}->{+(localtime( time + $day*86400 ))[6]};

    ($menu) = $html =~ /^\s*<b>\s*$den[^<]*<\/b>(.*)$/mi;
    $menu =~ s/<tr>//g;
    $menu =~ s/<\/tr>/\n/g;
    $menu =~ s/^.*Polévka/Polévka/s;
    $menu =~ s/&nbsp;?/ /g;
    $menu =~ s/<td>(\d+[^<]*Kč)<\/td>/ [$1]/g;
    $menu =~ s/<\/?td>//g;
    $menu =~ s/\)\s+\[/) [/g;

    # success!
    return (0, $menu);
}

1;
