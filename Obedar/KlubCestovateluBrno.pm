package Obedar::KlubCestovateluBrno;

use open ':std', ':utf8';

use strict;
use utf8;
use LWP::Simple;
use POSIX;

# my name
sub my_name {
    return 'klubcestovatelu';
}

# my URL
sub my_url {
    return 'http://www.hedvabnastezka.cz/klub-cestovatelu-brno/poledni-menu-2/';
}

# get_menu
sub get_menu {
    my ($day) = @_;
    my $menu;
    # get html/xml/source
    my $html = get( my_url );
    # fail :/
    return (1, 'parsing has not saved the day today') unless $html;
    # do your magic here
    $html =~ s/\r//g;
    $html =~ s/^.*strong>POLEDNÍ[^<]*<\/strong><\/p>//s;
    $html =~ s/<hr class="cleaner.*//s;
    $html =~ s/.*\n([^\n]*Cena poledního menu[^\n]*)\n/$1/s;

    my %ceny;
    my ($ceny) = $html =~ /^([^\n]*)\n/s;
    $ceny =~ s/<\/?(?:p|strong)>//g;
    %ceny = $ceny =~ /(\d+)/g;
    foreach my $c ( keys %ceny ) {
	$ceny{$c} .= ',-';
    }

    $html =~ s#^[^\n]*Cena poledního menu[^\n]*\n##s;
    $html =~ s#<p>&nbsp;</p>##g;
    $html =~ s#<p><strong><span style="text-decoration: underline;">\s*</span></strong></p>##g;
    $html =~ s/<p>Restaurace Klub.*//s;

    $html =~ s/<\/?(?:strong|span)[^>]*>//g;
    $html =~ s/<ol>\s*//g;
    $html =~ s/<\/ol>[^\n]*//g;

    $html =~ s/<\/(li|p)>\n/<\/$1>/sg;

    # rozbite html
    $html =~ s/(<\/li>)\s*\n\s*(<p>\s*\d\. )/$1<p>/sg;

    my $den = lc {
	1 => 'PONDĚLÍ',
	2 => 'ÚTERÝ',
	3 => 'STŘEDA',
	4 => 'ČTVRTEK',
	5 => 'PÁTEK',
    }->{+(localtime( time + $day*86400 ))[6]};


    my ($dnes) = $html =~ /^<p>$den[^<]*<\/p>([^\n]*)/m;
    $dnes =~ s/<(?:p|li)>\s*//g;
    $dnes =~ s/<\/(?:p|li)>/\n/g;

    my $i=0;
    $menu = '';
    foreach my $dl ( split(/\n/, $dnes) ) {
	# prvni je polevka v cene
	$menu .= $dl.( $i ? ' ['.$ceny{$i}.']' : '' )."\n";
	$i++;
    }

    # success!
    return (0, $menu);
}

1;

# vim: softtabstop=4
