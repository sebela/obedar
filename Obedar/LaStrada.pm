package Obedar::LaStrada;

use open ':std', ':utf8';

use strict;
use utf8;
use LWP::Simple;
use POSIX;

# my name
sub my_name {
	return 'lastrada';
}

# my URL
sub my_url {
	return 'http://lastrada-brno.cz/poledni-menu/';
}

# get_menu
sub get_menu {
	my ($day) = @_;
	my $menu;
	# get html/xml/source
	my $html = get( my_url );
	# do your magic here
	$html =~ s/<p><!--\s*more.*//s;
	$html =~ s/.*Polední menu ke stažení.*<\/a>\s*<\/p>//s;

	# ceny
	# Menu č.: 1                95Kč                                       Menu č.: 2, 3, 4           105Kč
	my %ceny;
	my ($c) = $html =~ /(Menu \S+\.:.*Kč)/;
	$c =~ s/\s+/ /g;
	foreach my $m ( map { $_ =~ s/^\s*//; $_ =~ s/\s*$//; $_.'Kč'; } split(/Kč/, $c) ) {
		$m =~ s/Menu \S+\.:\s*//;
		my ($cena) = $m =~ /(\d+Kč)\s*$/;
		$m =~ s/\s*$cena\s*$//;
		foreach my $cp ( split(/\s*,\s*/, $m) ) {
			$ceny{$cp} = $cena;
		}
	}

	$html =~ s/<p>(PONDĚLÍ|ÚTERÝ|STŘEDA|ČTVRTEK|PÁTEK)/\n<p>$1/g;
	$html =~ s/.*<div id="zvyhodnene">//s;
	$html =~ s/<\/div>.*//s;
	$html =~ s/<p>&nbsp;<\/p>//g;
	$html =~ s/>\r?\n/>/sg;
	$html =~ s/^\s*//s;
	$html =~ s/^<p>//gm;
	$html =~ s/<\/p>$//gm;

	my $den = {
		1 => 'PONDĚLÍ',
		2 => 'ÚTERÝ',
		3 => 'STŘEDA',
		4 => 'ČTVRTEK',
		5 => 'PÁTEK',
	}->{+(localtime( time + $day*86400 ))[6]};

	($menu) = $html =~ /^$den\s+(\S.*)$/m;
	my ($menu_pizza) = $html =~ /(3a..*)$/m;
	$menu =~ s/,\s*viz\.? níže/\n$menu_pizza/;

	$menu =~ s/<\/p><p>/\n/sg;

	# pridat ceny ke kazdemu "^\d\." radku
	foreach my $cp ( keys %ceny ) {
		$menu =~ s/^($cp.*)$/$1 [$ceny{$cp}]/m;
	}

	return (0, $menu);
}

1;
