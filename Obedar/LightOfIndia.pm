package Obedar::LightOfIndia;

# vim: tabstop=4 softtabstop=4 expandtab shiftwidth=4

use strict;
use utf8;
use LWP::Simple;
use POSIX;

# my name
sub my_name {
    return 'lightofindia';
}

# my URL
sub my_url {
    return 'http://lightofindia.cz/lang-cs/denni-menu';
}

# get_menu
sub get_menu {
    my ($day) = @_;
    my $menu;
    # get html/xml/source
    my $html = get( my_url );
    # fail :/
    return (1, 'parsing has not saved the day today') unless $html;

    # do your magic here
    $html =~ s/\r//g;
    $html =~ s/\n[^\n]*Obsažené alergeny.*//s;
    $html =~ s/.*H5>.*Týden:[^\n]*\n//s;
    $html =~ s/\x{feff}//g;
    $html =~ s/(<h2>)/\n$1/ig;

	my $den = {
	    1 => 'POND[ĚE]L[ÍI]',
		2 => '[ÚU]TER[ÝY]',
		3 => 'ST[ŘR]EDA',
		4 => '[ČC]TVRTEK',
		5 => 'P[ÁA]TEK',
	}->{+(localtime( time + $day*86400 ))[6]};

    ($menu) = $html =~ /$den\s*<\/[^>]*>\D+(.*)$/im;
    $menu =~ s/(<br[^>]*>)*$/\n/m;
    $menu =~ s/<br[^>]*>/\n/g;
    $menu =~ s/(\d+\s?Kč)/[$1]/g;
    $menu =~ s/\].*$/]/mg;

    # success!
    return (0, $menu);
}

1;
