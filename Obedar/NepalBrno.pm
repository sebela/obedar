package Obedar::NepalBrno;

use open ':std', ':utf8';

use strict;
use utf8;
use LWP::UserAgent;
use POSIX;

our $ua;

# my name
sub my_name {
    return 'nepal';
}

# my URL
sub my_url {
    return 'http://nepalbrno.cz/weekly-menu/';
}

# get_menu
sub get_menu {
    my ($day) = @_;
    my $menu;
    unless ( $ua ) {
	$ua = LWP::UserAgent->new;
	$ua->agent( 'Firefox' );
    }
    # get html/xml/source
    my $html = $ua->get( my_url )->decoded_content;
    # fail :/
    return (1, 'parsing has not saved the day today') unless $html;
    # do your magic here

    $html =~ s/\r//g;

    $html =~ s/<td colspan="3"><strong>Obsažené.*//s;
    $html =~ s/^.*<!--\s*Monday/<p><!--Monday/s;

    $html =~ s/<strong>\s*(\d+Kč)\s*<\/strong>/[$1]/g;
    $html =~ s/(\d)\)\s*\n/$1)/sg;
    $html =~ s/<\/td>\n/<\/td>/sg;
    $html =~ s/<\/?(?:strong|span|tr)[^>]*>//g;

    $html =~ s/<p><!--[^-]*-*-><\/p>/%CRLF%/sg;
    $html =~ s/\n\n*/\n/sg;

    $html =~ s/<\/td><td>/ /g;
    $html =~ s/<td[^>]*>//g;

    $html =~ s/<\/td>\n/%BR%/sg;

    $html =~ s/^\s+//mg;

    $html =~ s/%CRLF%\n/\n/sg;

    $html =~ s/%BR%$//mg;

    my $den = {
	1 => 'monday',
	2 => 'tuesday',
	3 => 'wednesday',
	4 => 'thursday',
	5 => 'friday',
    }->{+(localtime( time + $day*86400 ))[6]};

    ($menu) = $html =~ /^(.*$den.*)$/mi;
    $menu =~ s/%BR%/\n/g;
    $menu =~ s/^[^\n]*\n//s;
    # success!
    return (0, $menu);
}

1;
