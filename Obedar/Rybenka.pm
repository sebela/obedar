package Obedar::Rybenka;

# vim: tabstop=4 softtabstop=4 expandtab shiftwidth=4

use strict;
use utf8;
use LWP::Simple;
use POSIX;

# my name
sub my_name {
    return 'rybenka';
}

# my URL
sub my_url {
    return 'http://www.rybenka.cz/';
}

# get_menu
sub get_menu {
    my ($day) = @_;
    my $menu;
    # get html/xml/source
    my $html = get( my_url );
    # fail :/
    return (1, 'parsing has not saved the day today') unless $html;
    # do your magic here

    $html =~ s/^.*(<!--\s*začátek oddílu pondělí)/$1/s;
    $html =~ s/(konec oddílu pátek-->).*$//s;

    $html =~ s/>\n/>/sg;
    $html =~ s/\s*<tr class="alergeny">[^\n].*?<\/tr>//sg;
    $html =~ s/>\s*</></sg;
    $html =~ s/-->/-->\n/g;

	my $den = {
		1 => 'PONDĚLÍ',
		2 => 'ÚTERÝ',
		3 => 'STŘEDA',
		4 => 'ČTVRTEK',
		5 => 'PÁTEK',
	}->{+(localtime( time + $day*86400 ))[6]};

    $html =~ s/<!--.*?-->//g;
    ($menu) = $html =~ />$den[^<]*<\/td><\/tr>(.*)/mi;
    $menu =~ s/<\/tr>/<\/tr>\n/g;
    $menu =~ s/<\/?tr[^<]*>//g;
    $menu =~ s/<td>\s*([^<]*Kč)\s*<\/td>/ [$1]/g;
    $menu =~ s/^<td[^>]*>[^<]*<\/td>//gm;
    $menu =~ s/<\/?td[^>]*>//g;

    $menu =~ s/^\s+//gm;

    $menu =~ s/<!--.*?(?:-->)?\s*$//;

    # success!
    return (0, $menu);
}

1;
