package Obedar::Vitalite;

# vim: tabstop=4 softtabstop=4 expandtab shiftwidth=4

use strict;
use utf8;
use LWP::Simple;
use POSIX;

# my name
sub my_name {
    return 'vitalite';
}

# my URL
sub my_url {
    my $dow = strftime('%u', localtime);
    my $dow_map = {
        1 => 'Po',
        2 => 'Ut',
        3 => 'St',
        4 => 'Ct',
        5 => 'Pi',
    };
    if ($dow > 5) {
        return '';
    } else {
        return ('http://www.vitalmenu.cz/prodej-jidel/user-login-demo.php?login=jllidicka&pass=jllidicka&rlink=index.php?idrovnitkoF_HOTJendovkacatrovnitkoproduct_colorendovkadweekrovnitko' . $dow_map->{$dow});
    }
}

# get_menu
sub get_menu {
    my ($day) = @_;
    my $menu = my_url();
    # get html/xml/source
    # do your magic here
    if (not $menu) {
        # fail :/
        return (1, 'parsing has not saved the day today');
    } else {
        # success!
        return (0, $menu);
    }
}

1;
