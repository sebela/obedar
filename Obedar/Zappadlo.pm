package Obedar::Zappadlo;

use open ':std', ':utf8';

use strict;
use utf8;
use LWP::Simple;
use POSIX;

# my name
sub my_name {
    return 'zappadlo';
}

# my URL
sub my_url {
    return 'http://www.zappadlo.cz/denni-menu/';
}

# get_menu
sub get_menu {
    my ($day) = @_;
    my $menu;
    # get html/xml/source
    my $html = get( my_url );
    return (1, 'parsing has not saved the day today') unless $html;
    # do your magic here

    $html =~ s/\r//g;

    ($html) = $html =~ /(<article.*<\/article>)/s;
    
    $html =~ s/<!--[^>]*>//g;
    $html =~ s/<!--[^-]-->//g;
    $html =~ s/<!\[endif\]-->//g;
    
    $html =~ s/^.*<arcticle/<article/s;
    $html =~ s/<\/article>.*//s;

    $html =~ s/^.*<\/header>\s*//s;
    $html =~ s/<HR[^>]*>\s*<HR[^>]*>\s*<div class="jidlo_popis">Menu.*//si;

    $html =~ s/\s*<HR[^>]*>\s*//g;
    $html =~ s/<div class="entry-content">\s*//s;

    $html =~ s/<td[^>]*>/<td>/g;
    $html =~ s/<td><\/td>\s*//g;
    $html =~ s/<td>Menu\s+\d*\s*<\/td>\s*//g;
    $html =~ s/<td>(\d+\s*,-)\s*<\/td>/<td>[$1]<\/td>/g;

    $html =~ s/<br\s*\/>\n//sg;

    $html =~ s/<\/?table[^>]*>//g;
    $html =~ s/<\/?tr>//g;

    $html =~ s/\n\n*/\n/sg;
    $html =~ s/<\/td>\n/<\/td>/sg;
    $html =~ s/<\/div>\n/<\/div>/sg;

    $html =~ s/<div/\n<div/g;

    my $den = {
	1 => 'PONDĚLÍ',
	2 => 'ÚTERÝ',
	3 => 'STŘEDA',
	4 => 'ČTVRTEK',
	5 => 'PÁTEK',
    }->{+(localtime( time + $day*86400 ))[6]};

    ($menu) = $html =~ /^<[^>]*>$den[^<]*<\/div>(.*)$/m;

    $menu =~ s/<td>//g;
    $menu =~ s/<\/td>\[/ [/g;
    $menu =~ s/<\/td>/\n/g;

    # success!
    return (0, $menu);
}

1;
