# Obedar

Obedar je nastroj, ktory umoznuje zjednodusit rozhodovaci proces kam na obed.
Ide o bota, ktory sa dokaze pripojit na Jabber server a po 

Bot asi nie je pouzitelny uplne genericky a v kode obsahuje nejake specifika
suvisiace s fakultnym Jabber serverom (chat.fi.muni.cz), ale ide skor
o drobnosti.

Upozornenie: Obedar je mozne pripojit do MUC aj pod inym beznym uctom
cloveka, ale nie je to odporucane, kedze je tam riziko toho, ze potom budu
obedarovi prepadavat aj normalne spravy urcene pre bezny ucet. Asi sa mi to
nepodarilo uplne spolahlivo vykonfigurovat ani cez priority.

## Pouzivanie

Obedar sa po starte pripoji na zadany MUC kanal a pocuva na spravy, ktore
obsahuju string @obed. Pouzit ho je mozne nasledovne:

Vypis napovedu:

    @obed help

Vypis zoznam definovanych restauracii

    @obed list

Vypis menu pre konkretnu retauraciu

    @obed buddha

Vypis menu pre vsetky restauracie

    @obed all

Vypis menu pre nahodnu restauraciu

    @obed rnd/random

Je tiez mozne vytvorit s nim sukromny chat, kde takisto reaguje na spravy.
V takom pripade nie je nutne uvodzovat dotazy pren stringom @obed, ale staci
uviest samotny retazec, ktory za nim nasleduje, t.j. napr. skratka nejakej
restauracie.

Pred retazcom @obed sa mozu vyskytovat lubovolne veci. Avsak vsetko za @obed
sa spracovava tak, ze sa odstrania vsetky whitspace znaky, jeden pripadny
otaznik na konci a cely vystup sa povazuje za kod restauracie.

U restauracie je mozne pouzivat skratky (beranek -> ubilehoberanka).

V pripade restauracie definovanej cez Zomato je mozne definovat
parameter fallback\_url, ktory sa v takom pripade zobrazi.

## Konfiguracia

Pre jeho sfunkcnenie je potrebne vytvorit subor Config/Cred.pm, ktory bude
obsahovat nasledovnu konfiguraciu citlivych udajov

    package Config::Cred;

    our $jabber_info = {
        'login' => 'login@domena',
        'pass' => 'ugrofinske zalospevy',
        'nick'  => 'obedar',
        'room'  => 'kanal@chat.server.fqdn',
    };

    # toto je kluc k Zomato API, ktory je mozne ziskat registraciou
    # na https://developers.zomato.com/api

    our $zomato_key = 'deadbeef';

    1;
