#!/usr/bin/perl

use warnings;
use strict;

use Data::Dumper;

die "Chyba nazov modulu na otestovanie" if @ARGV != 1;

my $module = $ARGV[0];

binmode *STDOUT, ':encoding(utf8)';

eval "use Obedar::$module";
my ($stat, $msg) = eval ('Obedar::' . $module . '::get_menu()');

print "*** Navratovy kod: $stat ***\n$msg\n";
