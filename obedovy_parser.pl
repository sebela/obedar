#!/usr/bin/perl

use warnings;
use strict;
use utf8;

use Data::Dumper;
use LWP::UserAgent;
use LWP::Simple;
use JSON;
use POSIX qw(strftime);
use AnyEvent;
use AnyEvent::XMPP::Client;
use AnyEvent::XMPP::Util qw(node_jid);
use AnyEvent::XMPP::Ext::Disco;
use AnyEvent::XMPP::Ext::MUC;

use Config::Cred;
use Config::Restaurants;

my $restaurants = $Config::Restaurants::restaurants;

my $jabber_info = $Config::Cred::jabber_info;

my $zomato_cache = {};

sub get_menu_zomato {
    my ($info) = @_;

    my $ua = LWP::UserAgent->new();
    $ua->default_header('Accept' => 'application/json');
    $ua->default_header('user_key' => $Config::Cred::zomato_key);
    my $response = $ua->get('https://developers.zomato.com/api/v2.1/dailymenu?res_id=' . $info->{'site_id'});
    # 400 pouzivaju ako odpoved, ak dana restauracia neexistuje
    if (not $response->is_success and $response->code != 400) {
        return (1, 'Failed to fetch URL: ' . $response->status_line);
    }

    my $json = $response->decoded_content;
    my $menu_data = eval { from_json($json) };
    if (not $menu_data) {
        return (1, 'Failed to parse JSON: ' . $@);
    }

    if ($menu_data->{'status'} ne 'success') {
        # {"code":400,"status":"Bad Request","message":"No Daily Menu Available"}
        if ($menu_data->{'message'} eq 'No Daily Menu Available') {
            return (0, 'Kurwa! Nie mogłem znaleźć obiedove menu.');
        } else {
            return (1, 'Failed to get menu: ' . $json);
        }
    }

    my $daily_menu = $menu_data->{'daily_menus'}->[0]->{'daily_menu'};
    if (not defined $daily_menu) {
        return (1, 'Kurwa! Restauracje pewnie zapomniał wypełnić menu (brakujące element daily_menu).');
    }

    if ($daily_menu->{'start_date'} ne strftime('%Y-%m-%d 00:00:00', localtime)) {
        return (1, 'Start date of the menu is not today');
    }

    my $dishes = $daily_menu->{'dishes'};
    if (not defined $dishes) {
        return (1, 'Failed to process returned JSON: Element dishes not found');
    }

    $dishes = [ map { $_->{'dish'} } @$dishes ];

    if (not scalar @$dishes) {
        return (1, 'No dishes found');
    }

    my $menu = join "\n", map { ($_->{'name'} // '???') . ($_->{'price'} ? (' [' . $_->{'price'} . ']') : '') } @$dishes;
    $menu =~ s/[ \t]+/ /g;

    return (0, $menu);
}

sub get_menu_zomato_cached {
    my ($info) = @_;

    my $site = $info->{'site_id'};
    my $today = strftime('%Y-%m-%d', localtime);

    if (not exists $zomato_cache->{$site}) {
        goto FETCH;
    }

    my $data = $zomato_cache->{$site};

    if ($data->{'day'} ne $today) {
        goto FETCH;
    }

    print STDERR "Zomato cache hit for $site\n";
    return (0, $data->{'result'});

    FETCH:

    print STDERR "Zomato cache miss for $site\n";
    my ($code, $result) = get_menu_zomato($info);
    if ($code == 0) {
        $zomato_cache->{$site} = {
            'day'       => $today,
            'result'    => $result,
        };
        return (0, $result);
    } else {
        my $fallback = defined $info->{fallback}
            ? "\n" . $info->{fallback}
            : '';
        return ($code, $result . $fallback);
    }
}

sub get_menu_showurl {
    my ($info) = @_;
    return (0, $info->{'menu_url'});
}

sub normalize_restaurant {
    my ($rest) = @_;
    $rest =~ s/ //g;
    $rest =~ s/\?$//;
    $rest = lc $rest;

    for my $key (keys %$restaurants) {
        if (grep { $_ eq $rest } @{ $restaurants->{$key}->{'aliases'} }) {
            return $key;
        }
    }

    return $rest;
}

sub get_menu {
    my ($restaurant) = @_;

    $restaurant = normalize_restaurant($restaurant);

    my $prenote = '';
    my $note = '';

    my $rand_rest;
    if ($restaurant eq 'random' or $restaurant eq 'rnd') {
        my @rest_list = keys %$restaurants;
        $restaurant = $rest_list[rand @rest_list];
        $prenote = '--- ' . $restaurant . " ---\n";
    } elsif (not exists ($restaurants->{$restaurant})) {
        return 'Error: Co to kurwa jest za restauracju?';
    }

    my $info = $restaurants->{$restaurant};

    if (defined $info->{'note'}) {
        $note = "\n*** POZNAMKA: " . $info->{'note'};
    }

    if ($info->{'type'} eq 'zomato') {
        my ($stat, $msg) = get_menu_zomato_cached($info);
        if ($stat) {
            return $prenote . 'Zomato error: ' . $msg . $note;
        } else {
            return $prenote . $msg . $note;
        }
    } elsif ($info->{'type'} eq 'showurl') {
        my ($stat, $msg) = get_menu_showurl($info);
        if ($stat) {
            return 'ShowURL error: ' . $msg;
        } else {
            return $prenote . $msg . $note;
        }
    } elsif ($info->{'type'} eq 'nomenu') {
        return $prenote . 'Si nenavyberas.' . $note;
    } elsif ($info->{'type'} eq 'module') {
        # TODO osetrit
        eval "use $info->{'module'}";
        my $fn = $info->{'module'} . '::get_menu()';
        my ($stat, $msg) = eval "$fn";
        $msg =~ s/^\s+//;
        $msg =~ s/\s+$//;
        if ($stat) {
            return $prenote . 'Error: ' . $msg . ($info->{'fallback_url'} ? "\nTry " . $info->{'fallback_url'} : '') . $note;
        } else {
            return $prenote . $msg . $note;
        }
    }
}

sub obedar_reply {
    my ($chat_type, $text) = @_;

    my $prefix;
    if ($chat_type eq 'private') {
        $prefix = '';
    } elsif ($chat_type eq 'group') {
        $prefix = '@obed ';
    }

    my $private_prefix = 0;
    if ($chat_type eq 'private' and $text =~ m/^\@obed /) {
        $private_prefix = 1;
        $text =~ s/^\@obed //;
    }

    my $reply;
    if ($text eq '@obed help') {
        $reply = 'obed help - help
obed list - list all restaurants
obed all - show all menus
obed Everest - show menu for everest
obed rnd/random - show menu for a random restaurant'
    } elsif ($text eq "${prefix}list") {
        $reply = join ', ', sort keys %$restaurants;
    } elsif ($text eq "${prefix}all") {
        $reply = join "\n", map { "--- $_ ---\n" . get_menu($_) } sort keys %$restaurants;
    } elsif ($text =~ m/$prefix(.*)/) {
        $reply = get_menu($1);
    }

    if ($private_prefix) {
        $reply = "Not necessary to use \@obed prefix in private conversions\n\n" . $reply;
    }

    return $reply;
}

binmode STDOUT, ":encoding(utf8)";
binmode STDERR, ":encoding(utf8)";

my $cl      = AnyEvent::XMPP::Client->new;
my $disco   = AnyEvent::XMPP::Ext::Disco->new;
my $muc     = AnyEvent::XMPP::Ext::MUC->new(disco => $disco);
my $exit    = AnyEvent->condvar;
$cl->add_extension($disco);
$cl->add_extension($muc);
# dokumentacia: http://search.cpan.org/~elmex/AnyEvent-XMPP-0.52/lib/AnyEvent/XMPP/Writer.pm#METHODS
$cl->set_presence('available', 'Na ukoncenie obedoveho marazmu', 64);
$cl->add_account($jabber_info->{'login'}, $jabber_info->{'pass'});
$cl->reg_cb(
    'session_ready' => sub {
        my ($cl, $acc) = @_;
        print "session ready\n";

        $muc->join_room($acc->connection, $jabber_info->{'room'}, $jabber_info->{'nick'} // node_jid($acc->jid));
        $muc->reg_cb(
            message => sub {
                my ($cl, $room, $msg, $is_echo) = @_;
                return if $is_echo or $msg->is_delayed;
                my $text = $msg->any_body;

                return if $text !~ m/\@obed /;

                my $from = $msg->from;
                $from =~ s{^unix_obedy\@chat\.fi\.muni\.cz/}{};
                $from =~ s{@.*$}{};
                print STDERR strftime('[0] %Y-%m-%d %H:%M:%S ', localtime) . $from . ': ' . $msg->any_body . "\n";

                my $reply = obedar_reply('group', $text);

                my $repl = $msg->make_reply;
                $repl->add_body($reply);
                $repl->send;
            },
            error => sub {
                my ($cl, $room, $error) = @_;
                print "MUC error: " . $error->text . "\n";
            },
            join_error => sub {
                my ($cl, $room, $error) = @_;
                print "MUC join_error: " . $error->text . "\n";
            },
            enter => sub {
                my ($cl, $room, $user) = @_;
                print "entered " . $room->jid . "\n";
            },
        );

    },
    disconnect => sub {
        my ($cl, $acc, $h, $p, $reas) = @_;
        print "disconnect ($h:$p): $reas\n";
        $exit->send;
    },
    error => sub {
        my ($cl, $acc, $err) = @_;
        print "ERROR: " . $err->string . "\n";
    },
    message => sub {
        my ($cl, $acc, $msg) = @_;

        return if $msg->is_delayed;
        my $text = $msg->any_body;

        # return if not defined $text or $text !~ m/\@obed /;

        my $from = $msg->from;
        $from =~ s{^unix_obedy\@chat\.fi\.muni\.cz/}{};
        $from =~ s{@.*$}{};
        print STDERR strftime('[1] %Y-%m-%d %H:%M:%S ', localtime) . $from . ': ' . $msg->any_body . "\n";

        my $reply = obedar_reply('private', $text);

        my $repl = $msg->make_reply;
        $repl->add_body($reply);
        $repl->send;
    },
);

$cl->start;

my $iterm = AE::signal(TERM => sub { print "SIGTERM\n"; $cl->disconnect(); $exit->send } );
my $iint = AE::signal(INT => sub { print "SIGINT\n"; $cl->disconnect(); $exit->send } );

$exit->wait;

exit 0;
